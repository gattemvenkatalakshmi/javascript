let singleQuotes = 'Single quotes';
let doubleQuotes = "Double quotes";
let backticks = `Backticks`;

let multilineString = `
   This is a
   multiline
   string
   
`;

const str = 'Hello, world!';
console.log(str.length); // 13


const str1 = 'Hello,';
const str2 = ' world!';
console.log(str1.concat(str2)); // Hello, world!

console.log(str.substring(7, 12)); // "world"

console.log(str.indexOf('world')); // 7

console.log(str.toUpperCase()); // "HELLO, WORLD!"
console.log(str.toLowerCase()); // "hello, world!"

console.log(str.trim()); // "Hello, world!"

console.log(str.split(' ')); // ["Hello,", "world!"]

console.log(str.replace('world', 'everyone')); // "Hello, everyone!"


console.log(str.includes('world')); // true



console.log(str.startsWith('Hello')); // true
console.log(str.endsWith('world!')); // true




console.log(singleQuotes, doubleQuotes, backticks);
