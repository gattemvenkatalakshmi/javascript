//  hoisting  default behavior of moving declarations to the top 
// of the current scope before code execution.


// Variables declared with var are hoisted to the top of their function scope, 
// while let and const are hoisted to the top of their block scope but are not initialized.

console.log(hoistedVar); // Output: undefined
var hoistedVar = "I'm hoisted";
console.log(hoistedVar); // Output: I'm hoisted


console.log(hoistedLet); // Error: Cannot access 'hoistedLet' before initialization
let hoistedLet = "I'm not hoisted like var";

console.log(hoistedConst); // Error: Cannot access 'hoistedConst' before initialization
const hoistedConst = "I'm not hoisted like var";



//  function hoisting

//  can call the function before its declaration.


hoistedFunction(); // Output: I'm hoisted

function hoistedFunction() {
  console.log("I'm hoisted");
}



