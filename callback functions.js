// Example of a callback functions
//Basic Call functions
function greeting(name, callback) {
  console.log(`Hello, ${name}!`);
  callback();
}

function sayGoodbye() {
  console.log('Goodbye!');
}

greeting('Alice', sayGoodbye);
//*Asynchronous Callback Function: */
function fetchData(callback) {
  setTimeout(function() {
    callback('Data fetched successfully');
  }, 1000);
}

function processData(data) {
  console.log('Processing data:', data);
}

fetchData(processData);
// ****callback functions *****
// Example of a callback function with parameters
function multiply(a, b, callback) {
  var result = a * b;
  callback(result);
}

function displayResult(result) {
  console.log('Result:', result);
}

multiply(3, 4, displayResult);

// Error Handling CallBack Functions
function divide(a, b, callback) {
  if (b === 0) {
    callback(new Error('Division by zero is not allowed'));
  } else {
    callback(null, a / b);
  }
}

function handleResult(err, result) {
  if (err) {
    console.error('Error:', err.message);
  } else {
    console.log('Result:', result);
  }
}

divide(10, 0, handleResult);
// Asynchronous CallBack Functions
function asyncFunc1(callback) {
  setTimeout(function() {
    console.log('Async function 1');
    callback('Result from asyncFunc1');
  }, 1000);
}

function asyncFunc2(data, callback) {
  setTimeout(function() {
    console.log('Async function 2');
    callback('Result from asyncFunc2');
  }, 1000);
}

function asyncFunc3(data, callback) {
  setTimeout(function() {
    console.log('Async function 3');
    callback('Result from asyncFunc3');
  }, 1000);
}

// Nested callback functions
asyncFunc1(function(result1) {
  asyncFunc2(result1, function(result2) {
    asyncFunc3(result2, function(result3) {
      console.log('Final result:', result3);
    });
  });
});