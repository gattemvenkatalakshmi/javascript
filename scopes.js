// Scope determines the accessibility of variables and functions in different parts of the code. 
// JavaScript has two main types of scope: global scope and local scope.


//  global scope: can be accessesd anywhere in the code
let globalVar = "I'm a global variable";

function displayGlobalVar() {
  console.log(globalVar);
}

displayGlobalVar(); 


// local scope :can only be accessed within that function.

function localScopeExample() {
  let localVar = "I'm a local variable";
  console.log(localVar);
}

localScopeExample(); // Output: I'm a local variable
console.log(localVar); // Error: localVar is not defined



function myFunction() {
  
    var pizzaName = "Volvo";
    
    
  }
  const color = 'blue';

function printColor() {
  console.log(color);
}

 console.log(printColor());

// Variables declared with let or const within a block (e.g., inside an if statement or a loop)
//  have block scope.


  const isLoggedIn = true;

if (isLoggedIn == true) {
  const statusMessage = 'User is logged in.';
}

console.log(statusMessage);
