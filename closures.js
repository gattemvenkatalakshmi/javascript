
// A closure is a function that retains access to its outer (enclosing)
//  function's variables even after the outer function has returned.
//  Closures are created every time a function is created.




function outerFunction() {
    let outerVar = "I'm an outer variable";
  
    function innerFunction() {
      console.log(outerVar);
    }
  
    return innerFunction;
  }
  
  const closureExample = outerFunction();
  closureExample(); // Output: I'm an outer variable

  

// Closures are often used to create private variables or functions.

function counter() {
    let count = 0;
  
    return function () {
      count++;
      return count;
    };
  }
  
  const increment = counter();
  console.log(increment());
  console.log(increment()); 
  console.log(increment()); 
  