let exampleObject = { name: "John", age: 25, country: "USA" };

// Object.entries makes everything into arrays both keys and values
console.log(Object.entries(exampleObject));

// Object.keys keys into arrays
console.log(Object.keys(exampleObject));

// Object.values values into arrays
console.log(Object.values(exampleObject));

// hasOwnProperty gives true or false  whether objects.has value or not
console.log(exampleObject.hasOwnProperty("age"));

// Object.create 
let newObject = Object.create(exampleObject);
console.log(newObject.name);

// Object.assign

let mergedObject = Object.assign( exampleObject,  {occupation: "Engineer"} );
console.log(mergedObject);
let person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};
console.log(Object.entries(person));
console.log(Object.values(person));
console.log(Object.keys(person));

//objects spread operator

const obj1 = { foo: 'bar', x: 42 };
const obj2 = { ...obj1, y: 10 };
console.log(obj2); // { foo: 'bar', x: 42, y: 10 }

const merged_object = { ...obj1, ...obj2 }; 
console.log(merged_object)


