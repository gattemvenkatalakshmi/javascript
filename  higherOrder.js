


//  higher order functions
//  Takes one or more functions as arguments.
// Returns a function as its result.



// map function, which takes a function as an argument and applies it to each element in an array.

const numbers = [1, 2, 3, 4, 5];
const doubled = numbers.map(num => num * 2);
console.log(doubled); // Output: [2, 4, 6, 8, 10]


//  filter function Creates a new array with all elements that pass the test implemented by the provided function.

const numbers2 = [1, 2, 3, 4, 5];
const evens = numbers2.filter(num => num % 2 === 0);
console.log(evens); // Output: [2, 4]


//reduce functions on each element of the array, resulting in a single output value.


const numbers3 = [1, 2, 3, 4, 5];
const sum = numbers3.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
console.log(sum); // Output: 15


// for each is just  like for loop


const numbers4 = [1, 2, 3, 4, 5];
numbers4.forEach(num => console.log(num));


// some other higher order functions like which returns function  

function createMultiplier(multiplier) {
    return function (x) {
      return x * multiplier;
    };
  }
  
  const double = createMultiplier(2);
  console.log(double(5)); 
  
  const triple = createMultiplier(3);
  console.log(triple(5)); 
  

  // function  composition - combines two or more functions to produce new function



const compose = (f, g) => x => f(g(x));

const add2 = x => x + 2;
const multiply3 = x => x * 3;

const addThenMultiply = compose(multiply3, add2);
console.log(addThenMultiply(4)); 
