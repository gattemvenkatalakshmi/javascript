let exampleString = "   Hello, World!   ";

// toUpperCase
console.log(exampleString.toUpperCase());

// toLowerCase
console.log(exampleString.toLowerCase());

// trim
console.log(exampleString.trim());

// trimEnd
console.log(exampleString.trimEnd());

// trimStart
console.log(exampleString.trimStart());

// concat
let additionalString = " How are you?";
console.log(exampleString.concat(additionalString));

// endsWith
console.log(exampleString.endsWith("World!   "));

// includes
console.log(exampleString.includes("Hello"));

// indexOf
console.log(exampleString.indexOf("o"));

// lastIndexOf
console.log(exampleString.lastIndexOf("o"));

// padEnd
console.log(exampleString.padEnd(30, "*"));

// padStart
console.log(exampleString.padStart(30, "*"));

// repeat
console.log(exampleString.repeat(3));

// replace
console.log(exampleString.replace("World", "Universe"));

// slice
console.log(exampleString.slice(3, 7));

// split
console.log(exampleString.split(","));

// substring
console.log(exampleString.substring(3, 8));
//difference between slice and substring
const str = 'Hello, World!';

console.log(str.slice(-6));        // Output: "World!"
console.log(str.substring(-6));    // Output: "Hello, World!"
