 const fs = require('fs');
const csv = require('csv-parser');
 const filePath = ('./data/matches.csv')
  const dataJsonPath = ('./data/matches.json')
 const matchesdata =[];

fs.createReadStream(filePath).pipe(csv()).on("data" ,(row) => {matchesdata.push(row)})
 .on("end", function() { //console.log(matchesdata);
    writeToJsonFile(matchesdata, dataJsonPath)
     readDataFromJson(dataJsonPath);
    //   return result;
})
.on("error", function(){
     console.log("error")
})
 function writeToJsonFile(matchesdata, dataJsonPath){ 
     const jsondata = JSON.stringify(matchesdata, null,2);
     fs.writeFileSync(dataJsonPath, jsondata)
   
 }
  function readDataFromJson(dataJsonPath){
     const data =fs.readFileSync(dataJsonPath, 'utf-8');
      const data2 = JSON.parse(data);
       console.log(data2);
  }