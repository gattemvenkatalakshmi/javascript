const sum = (firstParam, secondParam) => { 
    return firstParam + secondParam; 
  }; 
  console.log(sum(2,5)); // Prints: 7 
  
  // Arrow function with no parameters 
  const printHello = () => { 
    console.log('hello'); 
  }; 
  printHello(); // Prints: hello
  
  // Arrow functions with a single parameter 
  const checkWeight = weight => { 
    console.log(`Baggage weight : ${weight} kilograms.`); 
  }; 
  checkWeight(25); // Prints: Baggage weight : 25 kilograms.
  
  
  // Concise arrow functions
  const multiply = (a, b) => a * b; 
  console.log(multiply(2, 30)); // Prints: 60 
  function suma(num1, num2) {
    return num1 + num2;
  }
  
  // Calling the function:
  suma(3, 6); // 9
  function rocketToMars() {
    return 'BOOM!';
  }
  
  // Anonymous function
  const RocketToMars = function() {
    return 'BOOM!';
  }
  const dog = function() {
    return 'Woof!';
  }
  function sayHello(name) {
    return `Hello, ${name}!`;
  }
  


  // what happens when a function does not have a return statement

  // output will be undefinied
  function noReturn() {
    let a = 10;
    let b = 20;
    let sum = a + b;
  }
  
  let result = noReturn();
  console.log(result); 
  
  
  
  function add(num1, num2) {
    return num1 + num2;
  }
  
  // Calling the function
  sum(2, 4); // 6