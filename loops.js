for (let i = 0; i < 5; i++) {
    console.log(i);
  }
  
  // While Loop
  let j = 0;
  while (j < 5) {
    console.log(j);
    j++;
  }
  
  // For...of Loop
  let iterable = [1, 2, 3];
  for (let value of iterable) {
    console.log(value);
  }
  
  // For...in Loop (for objects)
  let person = { name: 'John', age: 30 };
  for (let key in person) {
    console.log(key, person[key]);
  }