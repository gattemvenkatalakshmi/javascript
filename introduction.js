let number = 100;

// Both statements will add 10
number = number + 10;
number += 10;

console.log(number); 
// Prints: 120
let age = 7;

// String concatenation
console.log('Tommy is ' + age + ' years old.');

// String interpolation
console.log(`Tommy is ${age} years old.`);
const currency = '$';
let userIncome = 85000; 

console.log(currency + userIncome + ' is more than the average income.');
// Prints: $85000 is more than the average income.
var a;

console.log(a); 
let name = "Tammy";
const found = false;
var age1 = 3;
console.log(name, found, age1);
// Prints: Tammy false 3
let name2= "Codecademy";
console.log(`Hello, ${name2}`); 
// Prints: Hello, Codecademy

console.log(`Billy is ${6+8} years old.`); 
// Prints: Billy is 14 years old.
let count; 
console.log(count); // Prints: undefined
count = 10;
console.log(count); // Prints: 10
const numberOfColumns = 4;
//numberOfColumns = 8;
let service = 'credit card';
let month = 'May 30th'; 
let displayText = 'Your ' + service  + ' bill is due on ' +  month + '.';

console.log(displayText);
console.log('Hi there!');
console.log(Math.random());
let amount = 6;
let price = 4.99;
console.log(Math.floor(5.95)); 
const weeksInYear = Math.floor(365/7);

// calcuates the number of days left over after 365 is divded by 7
const daysLeftOver = 365 % 7 ;

console.log("A year has " + weeksInYear + " weeks and " + daysLeftOver + " days")

//pass by reference - non primitive data types (objects and arrays)


function changeProperty(obj) {
    obj.name = "Alice";
  }
  
  let person = { name: "Bob" };
  changeProperty(person);
  console.log(person.name); // Output: Alice
  
// pass by value - primitive data types (string , boolean, Number, null)
function changeValue(x) {
    x = 10;
  }
  
  let a = 5;
  changeValue(a);
  console.log(a); // Output: 5

  
  // difference between === and ==
console.log(5 === 5);         // true (same type and value)
console.log(5 === '5');       // false (different types)
console.log(true === 1);      // false (different types)
console.log(null === undefined); // false (different types)

console.log(5 == 5);         // true (same value)
console.log(5 == '5');       // true (values are coerced to the same type)
console.log(true == 1);      // true (true is coerced to 1)
console.log(null == undefined); // true (null and undefined are considered equal)



