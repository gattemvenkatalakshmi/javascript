const numbers = [1, 2, 3, 4];

 console.log(numbers.length) // 4
 const myArray = [100, 200, 300];

console.log(myArray[0]); // 100
console.log(myArray[1]); // 200
console.log(myArray[2]); // 300
const cart = ['apple', 'orange'];
cart.push('pear'); 

// Adding multiple elements:
const numbers2 = [1, 2];
numbers2.push(3, 4, 5);
console.log(numbers2)
const ingredients = ['eggs', 'flour', 'chocolate'];

const poppedIngredient = ingredients.pop(); // 'chocolate'
console.log(ingredients); // ['eggs', 'flour']
const names = ['Alice', 'Bob'];

names.push('Carl');
console.log(names);
const numberArray = [0, 1, 2, 3];

// An array containing different data types
const mixedArray = [1, 'chicken', false];
let exampleArray = [1, 2, 3, 4, 5];

// join
console.log(exampleArray.join("-"));

// flat
let nestedArray = [1, [2, 3], [4, [5, 6]]];
console.log(nestedArray.flat(2));

// push
exampleArray.push(6);
console.log(exampleArray);

// indexOf
console.log(exampleArray.indexOf(3));

// lastIndexOf
console.log(exampleArray.lastIndexOf(3));

// includes
console.log(exampleArray.includes(3));

// reverse
console.log(exampleArray.reverse());

// every
console.log(exampleArray.every(num => num > 0));

// shift
console.log(exampleArray.shift());
console.log(exampleArray);

// splice
exampleArray.splice(2, 1, 10, 11);
console.log(exampleArray);

// find first element which satisifies gives condition
console.log(exampleArray.find(num => num > 4));

// unshift
exampleArray.unshift(0);
console.log(exampleArray);

// findIndex
console.log(exampleArray.findIndex(num => num > 4));

// filter
console.log(exampleArray.filter(num => num > 3));

// flat
console.log(exampleArray.flat());

// forEach
exampleArray.forEach(num => console.log(num));

// map
let squaredArray = exampleArray.map(num => num ** 2);
console.log(squaredArray);

// pop
console.log(exampleArray.pop());
console.log(exampleArray);

// reduce
let sum = exampleArray.reduce((acc, num) => acc + num, 0);
console.log(sum);

// slice
let slicedArray = exampleArray.slice(1, 4);
console.log(slicedArray);

// some
console.log(exampleArray.some(num => num > 10));

const arr1 = [1, 2, 3];
const arr2 = [...arr1, 4, 5, 6];
console.log(arr2); // [1, 2, 3, 4, 5, 6]

