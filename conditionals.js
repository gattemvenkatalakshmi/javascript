// Truthy values are all values that are not falsy. 
// falsy values are false, 0, "", null, undefined, or NaN 



let price = 10.5;
let day = "Monday";

day === "Monday" ? price -= 1.5 : price += 1.5;
console.log(price);
const isTaskCompleted = false;

if (isTaskCompleted) {
  console.log('Task completed');
} else {
  console.log('Task incomplete');
}
const food = 'salad';

switch (food) {
  case 'oyster':
    console.log('The taste of the sea ');
    break;
  case 'pizza':
    console.log('A delicious pie ');
    break;
  default:
    console.log('Enjoy your meal');
}
const isMailSent = true;

if (isMailSent) {
  console.log('Mail sent to recipient');
}
let lateToWork = true;
let oppositeValue = !lateToWork;

console.log(oppositeValue); 
// Prints: false
const size = 10;

if (size > 100) {
  console.log('Big');
} else if (size > 20) {
  console.log('Medium');
} else if (size > 4) {
  console.log('Small');
} else {
  console.log('Tiny');
}