//  Errors try  catch block 
try {
    let result = riskyOperation();
    console.log(result);
  } catch (error) {
    console.error("An error occurred:", error.message);
  } finally {
    console.log("Execution completed.");
  }
  




// difference between throw new Error("Error message here") and throw "Error message here


// throw new Error("Error message here"), you are throwing an instance of the Error
class CustomError extends Error {
    constructor(message) {
      super(message);
      this.name = "CustomError";
    }
  }
  
  try {
    throw new CustomError("Custom error occurred");
  } catch (e) {
    console.log(e.message); 
    console.log(e.name);    
    console.log(e.stack);   
  }
  


// throw new Error("Error message here"), you are throwing an instance of the Error

try {
    throw "Something went wrong";
  } catch (e) {
    console.log(e); // Output: Something went wrong
  }
  



//  Custom Errors

class ValidationError extends Error {
  constructor(message) {
    super(message);
    this.name = "ValidationError";
  }
}

function validateInput(input) {
  if (typeof input !== 'string') {
    throw new ValidationError("Invalid input: Expected a string");
  }
  // Additional validation logic
}

try {
  validateInput(123);
} catch (error) {
  if (error instanceof ValidationError) {
    console.error("Validation failed:", error.message);
  } else {
    console.error("An unexpected error occurred:", error.message);
  }
}

