// Synchronous
console.log('Start');
console.log('Middle');
console.log('End');

// Asynchronous
console.log('Start');
setTimeout(function() {
  console.log('Middle');
}, 1000);
console.log('End');
