let person = { name: 'John', age: 25 };

// Convert JavaScript object to JSON string
let jsonString = JSON.stringify(person);
console.log(jsonString);

// Parse JSON string back to JavaScript object
let parsedObject = JSON.parse(jsonString);
console.log(parsedObject);